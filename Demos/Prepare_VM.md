# On Ubuntu 

1. Update the package manager
```bash
sudo apt update
```

2. Install Snap
```bash
sudo apt install snapd
```

3. Install Microk8s 
```bash
sudo snap install microk8s --classic
```
4. Adding current user to docker group
```bash
sudo usermod -aG microk8s <username>
```
5. Install Kubectl 
```bash
sudo snap install kubectl --classic
```
6. Install helm
```bash
sudo snap install helm --classic
```
7. Install docker
```bash
# add docker repo to your package manager
## install key validation tools
sudo apt install apt-transport-https ca-certificates curl software-properties-common
## Trust server keys
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

## adding the docker servers to apt list
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
## update the package manager
sudo apt update
# install docker from docker official repos
sudo apt install docker-ce
```
8. Adding current user to docker group
```bash
sudo usermod -aG docker <username>
```

9. Install OpenSSH for smoother communication
```bash
sudo apt update
sudo apt install openssh-server
```

10. Edit Hosts file 
```bash
# Update hosts file
sudo nano /etc/hostsG
```