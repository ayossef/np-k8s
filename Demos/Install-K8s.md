# Install a Microk8s Cluster

## Steps:

1. Create 2 VMs
2. First VM will be used as Master
3. Second VM will be joining the cluster
4. Install Kubectl to manage the cluster from CLI
5. Enable the web interface (dashboard)
6. Install another managment tool (rancher) - as time permits
7. Installing differnt arch of PHP Apps
8. Creating a Helm Chart and installing it on the cluster
9. Monitoring, managing and Securing 